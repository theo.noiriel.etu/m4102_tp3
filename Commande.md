## Développement d'une ressource *Pizza*

### API et représentation des données


| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /commande             | GET         | <-application/json<br><-application/xml                      |                 | liste des commandes (C2)                                           |
| /commande/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une commande(C2) ou 404                                            |
| /commande/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la commande ou 404                                        |
| /commande            | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1)      | Nouvelle commande (C2)<br>409 si la commande existe déjà (même nom) |
| /commande/{id}        | DELETE      |                                                              |                 |                                                                      |

Une commande comporte uniquement un identifiant, un nom et une liste de pizzas. Sa
représentation JSON (C2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "patrick chirak",
      "pizzas" : ["Cannibale", "Montagnarde"]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente un ingrédient. Aussi on aura une
représentation JSON (C1) qui comporte uniquement le nom :

    { "name": "patrick chirak", "pizzas" : ["Cannibale", "Montagnarde"]}
    
#### JavaBeans
JavaBean est un standard pour les objets Java permettant de les créer
et de les initialiser et de les manipuler facilement. Pour cela ces
objets doivent respecter un ensemble de conventions :

  - la classe est sérialisable
  - elle fournit au moins un constructeur vide
  - les attributs privés de la classe sont manipulables via des
    méthodes publiques **get**_Attribut_ et **set**_Attribut_

Les DTO et la classe `Commande`décrits dans la suite sont des
JavaBeans.

#### Data Transfer Object (DTO)
Les DTO correspondent à la représentation des données qui sera
transportée par HTTP. Ce sont des Javabeans qui possèdent les même
propriétés que la représentation (avec les getter/setter
correspondants).

Jersey utilisera les *setter* pour initialiser l'objet à partir
de la représentation JSON ou XML et les *getter* pour créer la
représentation correspondante.

#### Data Access Object (DAO)
Le DAO permet de faire le lien entre la représentation objet et le
contenu de la base de données.

Nous utiliserons la [librairie JDBI](http://jdbi.org/) qui permet
d'associer une interface à des requêtes SQL.
La classe `BDDFactory` qui vous est fournie permet un accès facilité
aux fonctionnalités de JDBI.

#### La représentation des données manipulées par la ressource
La classe `Commande` est un JavaBean qui représente ce qu'est une
commande. Elle porte des méthodes pour passer de cette
représentation aux DTO.

Cela permet de découpler l'implémentation de la ressource qui traite
les requêtes HTTP et la donnée manipulée.

Cette classe pourrait
porter des comportements liés à cette donnée (par ex. calcul de TVA).

### Une première implémentation : récupérer les ingrédients existants
Nous allons réaliser un développement dirigé par les tests. Dans un
premier temps, nous allons commencer par un test qui récupère une
liste de pizzas vide qui sera matérialisée par un tableau JSON
vide `[]`.

Le code suivant qui se trouve dans la classe `CommandeResourceTest`
montre la mise en place de l'environnement (`configure()`) et l'amorce
d'un premier test.

    
