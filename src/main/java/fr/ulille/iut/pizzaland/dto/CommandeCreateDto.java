package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeCreateDto {
	private String name;
	private List<Pizza> pizzas;
		
	public CommandeCreateDto() {
		this.pizzas = new ArrayList<Pizza>();
	}
	
	public CommandeCreateDto(String name) {
		this.name = name;
		this.pizzas = new ArrayList<Pizza>();
	}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

	public List<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}

	@Override
	public String toString() {
		return "PizzaCreateDto [name=" + name + ", pizzas=" + pizzas + "]";
	}
	
	
}