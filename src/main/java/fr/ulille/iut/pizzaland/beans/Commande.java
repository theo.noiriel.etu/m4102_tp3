package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Commande {
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Pizza> pizzas;
	
	public Commande() {
		this.pizzas = new ArrayList<Pizza>();
	}
	
	public Commande(String name) {
		this.name = name;
		this.pizzas = new ArrayList<Pizza>();
	}

	public Commande(UUID id, String name) {
		this.id = id;
		this.name = name;
		this.pizzas = new ArrayList<Pizza>();
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}

	public static CommandeDto toDto(Commande commande) {
		CommandeDto dto = new CommandeDto();
		dto.setId(commande.getId());
		dto.setName(commande.getName());

		return dto;
	}

	public static Commande fromDto(CommandeDto dto) {
		Commande commande= new Commande();
		commande.setId(dto.getId());
		commande.setName(dto.getName());

		return commande;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commande other = (Commande) obj;
		if (id == null) {
			if (other.getId() != null)
				return false;
		} else if (!id.equals(other.getId()))
			return false;
		if (name == null) {
			if (other.getName() != null)
				return false;
		} else if (!name.equals(other.getName()))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", ingredients=" + pizzas + "]";
	}

	public static CommandeCreateDto toCreateDto(Commande commande) {
		CommandeCreateDto dto = new CommandeCreateDto();
		dto.setName(commande.getName());
		dto.setPizzas(commande.getPizzas());
		return dto;
	}

	public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
		Commande commande = new Commande();
		commande.setName(dto.getName());
		commande.setPizzas(dto.getPizzas());
		return commande;
	}
}